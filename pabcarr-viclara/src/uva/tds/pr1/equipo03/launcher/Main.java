package uva.tds.pr1.equipo03.launcher;

import java.util.Scanner;
import uva.tds.pr1.equipo03.test.Test;

/**
 * Launcher de la aplicacion
 * 
 * @author Pablo Carrascal Muñoz
 * @author Victor Lara Mongil
 */
public class Main {

    /**
     * Punto de inicio de la aplicacion.
     * Pide por consola un path de entrada y ejecuta los test de prueba
     * @param args Argumentos de entrada (ninguno)
     */
    public static void main(String [] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert here the xml path");
        Test test = new Test(scanner.nextLine());
        scanner.close();
        
        test.gettersTest();
        test.modifiedSysemTest();
        test.loadAndReloadTest();
    }
}