package uva.tds.pr1.equipo03.system;

/**
 * @author Pablo Carrascal Muñoz
 * @author Victor Lara Mongil
 * 
 * Clase enum que encapsula las posibles consolas que puede tener un {@link User}.
 */
public enum EnumShell {
    BASH ("bash"),
    ASH ("ash"),
    CSH ("csh"),
    ZSH ("Zsh"),
    KSH ("ksh"),
    TCSH ("tcsh");

    private final String name;

    /**
     * Devuelve el enum con la consola seleccionada.
     * @param name consola seleccionada.
     */
    EnumShell(String name) {
        this.name = name;
    }

    /**
     * Devuelve la consola seleccionada.
     * @return la consola seleccionada.
     */
    public String getValue() {
        return this.name;
    }
}
