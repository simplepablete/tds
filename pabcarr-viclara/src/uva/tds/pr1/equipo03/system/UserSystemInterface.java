package uva.tds.pr1.equipo03.system;

import java.nio.file.Path;

/**
 * @author Pablo Carrascal Muñoz
 * @author Victor Lara Mongil
 * 
 * Interface que define como cargar, modificar y guardar los datos del sistema
 * (usuarios y grupos) contenidos en un fichero con formato xml.
 */
public interface UserSystemInterface {
	
    /**
     * Carga un sistema con el contenido del fichero xml contenido en el path.
     * @param pathToXML ruta del fichero xml con el contenido del sistema.
     */
    void loadFrom(Path pathToXML);

    /**
     * Guarda un sistema cargado en un fichero xml en el path de destino.
     * @param pathToXML ruta del fichero xml donde guardar el contenido del sistema.
     */
    void updateTo(Path pathToXML);

    /**
     * Devuelve el estado (cargado o no) del sistema.
     * @return true si el sistema ha sido cargado y false en caso contrario.
     */
    boolean isXMLLoaded();

    /**
     * Devuelve el estado (modificado o no) del sistema.
     * @return true si el sistema ha sido modificado despues de la carga y false en caso contrario.
     */
    boolean isModifiedAfterLoaded();

    /**
     * Crea un nuevo usuario en el sistema con los datos proporcionados.
     * @param name nombre del usuario.
     * @param uId userId del usuario.
     * @param password contraseña del usuario.
     * @param pathToHome ruta al home del usuario.
     * @param fullName nombre completo del usuario.
     * @param shell consola del usuario.
     * @param mainGroup grupo principal del usuario.
     * @param secundaryGroups grupos secundarios del usuario.
     */
    void createNewUser(String name, int uId, String password, Path pathToHome, 
            String fullName, EnumShell shell, Group mainGroup, Group[] secundaryGroups);

    /**
     * Devuelve el usuario con el id proporcionado del sistema.
     * @param uId userId del usuario.
     * @return el usuario con el id proporcionado del sistema.
     * o null si el usuario no se encuentra en el sistema.
     */
    User getUserById(int uId);

    /**
     * Devuelve el usuario con el nombre proporcionado del sistema.
     * @param name nombre del usuario.
     * @return el usuario con el nombre proporcionado del sistema,
     * o null si el usuario no se encuentra en el sistema.
     */
    User getUserByName(String name);

    /**
     * Devuelve el grupo con el id proporcionado del sistema.
     * @param gId id del grupo.
     * @return el grupo con el id proporcionado del sistema.
     * o null si el grupo no se encuentra en el sistema.
     */
    Group getGroupById(int gId);

    /**
     * Devuelve el grupo con el nombre proporcionado del sistema.
     * @param name nombre del grupo.
     * @return el grupo con el nombre proporcionado del sistema,
     * o null si el grupo no se encuentra en el sistema.
     */
    Group getGroupByName(String name);

    /**
     * Crea un nuevo grupo en el sistema con los datos proporcionados.
     * @param name nombre del nuevo grupo.
     * @param gId id del nuevo grupo.
     */
    void createNewGroup(String name, int gId);

    /**
     * Añade un usuario del sistema a un grupo del sistema.
     * El usuario no debe pertenecer a ese grupo.
     * @param user usuario a añadir.
     * @param group grupo al que añadir el usuario.
     */
    void addUserToGroup(User user, Group group);

    /**
     * Elimina un usuario del sistema de un grupo del sistema.
     * El usuario debe pertenecer al grupo.
     * El grupo no debe ser el grupo principal del usuario.
     * @param user usuario a eliminar.
     * @param group grupo del que eliminar al usuario.
     */
    void removeUserFromGroup(User user, Group group);
    
    /**
     * Elimina un usuario del sistema.
     * @param user usuario a eliminar.
     */
    void removeUserFromSystem(User user);
    
    /**
     * Elimina un grupo del sistema.
     * El grupo a eliminar no puede ser el grupo principal de ningun usuario del sistema.
     * @param group grupo a eliminar del sistema
     */
    void removeGroupFromSystem(Group group);
}
