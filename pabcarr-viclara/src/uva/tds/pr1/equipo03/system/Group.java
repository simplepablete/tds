package uva.tds.pr1.equipo03.system;

import java.util.ArrayList;

/**
 * @author Pablo Carrascal Muñoz
 * @author Victor Lara Mongil
 * 
 * Clase que representa un grupo en el sistema.
 */
public class Group implements Comparable<Group> {
	
    private String name;
    private final int id;
    private final ArrayList<User> members;

    private static ArrayList<Group> systemGroups = new ArrayList<>();
    
    /**
     * Metodo protegido para reiniciar el sistema, eliminando todos los 
     * grupos que contenga.
     */
    protected static void resetSystemGroups () {
        systemGroups = new ArrayList<> ();
    }
    
    /**
     * Devuelve un {@link ArrayList} con todos los grupos en el sistema.
     * @return {@link ArrayList} con todos los grupos en el sistema.
     */
    public static ArrayList<Group> getSystemGroups() {
        return systemGroups;
    }

    /**
     * Devuelve la informacion de los grupos en el sistema en formato xml.
     * @return {@link String} con la informacion de los grupos en el sistema en formato xml.
     */
    public static String printSystemGroups () {
        String groups = "";
        
        for (Group group: Group.getSystemGroups()){
            groups += "    <group group_ID=\"G" + group.getId() + "\" ";
            
            if(!group.getMembers().isEmpty()){
                groups += "members=\"";
                for (User user: group.getMembers()){
                    groups += "U" + user.getId() + " ";
                }
                groups += "\" ";
            }
        
            groups += ">\n";
            groups += "        <group_name>" + group.getName() + "</group_name>\n";
            groups += "    </group>\n";
        }
        return groups;
    }

    private void validateData(String name, int id) throws IllegalArgumentException{
        if (name.equals("dummy")) {
            throw new IllegalArgumentException("Name \"dummy\" is reserved by the system. the operation failed.");
        }
        
        if (id < 1) {
            throw new IllegalArgumentException("Id must be positive. the operation failed.");
        }
        
        for (Group group: Group.getSystemGroups()) {
            if (group.getId() == id || group.getName().equals(name)) {
                throw new IllegalArgumentException("There is a group with the same id or name. The operation failed");
            }
        }
    }
    
    private void validateNewName (String name) throws IllegalArgumentException{
        if (name.equals("dummy")) {
            throw new IllegalArgumentException("Name \"dummy\" is reserved by the system. the operation failed.");
        }
        
        for (Group group: Group.getSystemGroups()) {
            if (group.getName().equals(name)) {
                throw new IllegalArgumentException("There is a group with the same name. The operation failed");
            }
        }
    }
    
    /**
     * Crea un nuevo grupo en el sistema con su nombre y su id. El nombre y el id
     * de un grupo en el sistema deben ser unicos. El id debe ser un entero positivo.
     * @param name nombre del nuevo grupo.
     * @param id id del nuevo grupo.
     * @throws IllegalArgumentException si el nombre o el id no cumple con los requisitos.
     */
    public Group(String name, int id) throws IllegalArgumentException {
        validateData(name, id);
        this.name = name;
        this.id = id;
        this.members = new ArrayList<>();
    }

    /**
     * Devuelve el nombre del grupo.
     * @return el nombre del grupo.
     */
    public String getName() {
        return name;
    }

    /**
     * Fija un nuevo nombre para el grupo en el sistema. 
     * El nuevo nombre debe ser unico en el sistema.
     * @param name nuevo nombre del grupo.
     */
    public void setName(String name) {
        try{
            validateNewName(name);
            this.name = name;
        } catch (IllegalArgumentException e) {
            System.err.println(e);
        }
    }

    /**
     * Devuelve el id del grupo en el sistema.
     * @return el id del grupo en el sistema.
     */
    public int getId() {
        return id;
    }

    /**
     * Fija un nuevo id para el grupo en el sistema.
     * El nuevo id debe ser unico en el sistema y ser un entero positivo
     * @param id nuevo id del grupo
     */
//    public void setId(int id) {
//        try {
//            validateData(this.name, id);
//            this.id = id;
//        } catch (IllegalArgumentException e) {
//            System.err.println(e);
//        }
//        
//    }

    /**
     * Devuelve los usuarios en el grupo.
     * @return {@link ArrayList} con los usuarios en el grupo.
     */
    public ArrayList<User> getMembers() {
        return members;
    }  

    /**
     * Añade un usuario al grupo.
     * El usuario no debe estar ya en el grupo.
     * @param user nuevo {@link User} del grupo.
     * @throws IllegalArgumentException si el usuario ya pertenece al grupo.
     */
    public void addUser(User user) throws IllegalArgumentException {
        for (User member: this.members) {
            if (member.getId() == user.getId()) {
                throw new IllegalArgumentException("This user is already in this group, the operation failed");
            }
        }
        this.members.add(user);
    }

    /**
     * Saca un usuario del grupo.
     * El usuario debe estar en el grupo.
     * @param user {@link User} a eliminar del grupo.
     * @throws IllegalArgumentException si el usuario no esta en el grupo
     */
    public void removeUser(User user) throws IllegalArgumentException {
        boolean isUserInGroup = false;
        
        for (User member: this.getMembers()) {
            if (member.getId() == user.getId()) {
                isUserInGroup = true;
            }
        }
        if (!isUserInGroup) {
            throw new IllegalArgumentException("This user isn't in this group, the operation failed");
        }
        
        if (user.getMainGroup().getId() == this.getId()) {
            throw new IllegalArgumentException("Can't remove the user from his main group. The operation failed");
        }
        this.members.remove(user);
    }
    
    /**
     * Implementacion de la interfaz Comparable\<Group\> que permite comparar
     * grupos respecto de su id.
     * @param group {@link Group} a comparar.
     * @return el resultado de restar los ids de los grupos. 
     */
    @Override
    public int compareTo(Group group) {
        return (this.id - group.getId());
    }
}
