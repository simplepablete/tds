package uva.tds.pr1.equipo03.system;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * @author Pablo Carrascal Muñoz
 * @author Victor Lara Mongil
 * 
 * Implementacion de la interfaz que permite cargar, modificar y guardar los datos del sistema
 * (usuarios y grupos) contenidos en un fichero con formato xml.
 */
public class UserSystemImpl implements UserSystemInterface {

    private final String GROUP_TAG = "group";
    private final String USER_TAG = "user";
    private final String XML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    private final String XML_DTD = "<!DOCTYPE groups_users_unix SYSTEM \"src/uva/tds/pr1/equipo03/xml/groups_users_unix.dtd\">";
    private final String ROOT_TAG = "groups_users_unix";

    private boolean xmlLoaded = false;
    private boolean modifiedAfterLoaded = false;
    private boolean errorOnLoad = false;

    
    private void prepareGroupToRemoveFromSystem(Group group) throws IllegalArgumentException {
        if (!canRemoveGroupFromSystem(group)) {
            throw new IllegalArgumentException("Can't remove a group used by main group for an user. The operation failed");
        }
    }

    private void parseGroups(NodeList unparsedGroups) {
        for(int i = 0; i < unparsedGroups.getLength(); i++) {
            NamedNodeMap attrMap = unparsedGroups.item(i).getAttributes();
            NodeList elements = unparsedGroups.item(i).getChildNodes();

            String sId = attrMap.getNamedItem("group_ID").getNodeValue();
            int id = Integer.parseInt(sId.split("G")[1]);

            String name = null;
            for (int j = 0; j < elements.getLength(); j++) {
                Node node = elements.item(j);
                if (node.getNodeName().equals("group_name")) {
                    name = node.getTextContent();
                }
            }
            createNewGroup(name, id);
        }
    }

    private void parseUsers(NodeList unparsedUsers) {
        String stringAux;
        String [] stringAux2;
        int intAux;
        ArrayList<Group> secondaryGroups;
        Node nodeAux;

        for(int i = 0; i < unparsedUsers.getLength(); i++) {
            NamedNodeMap attrMap = unparsedUsers.item(i).getAttributes();
            NodeList elements = unparsedUsers.item(i).getChildNodes();

            stringAux = attrMap.getNamedItem("user_ID").getNodeValue();
            int userId = Integer.parseInt(stringAux.split("U")[1]);

            stringAux = attrMap.getNamedItem("main_group").getNodeValue();
            intAux = Integer.parseInt(stringAux.split("G")[1]);
            Group mainGroup = getGroupById(intAux);

            stringAux = attrMap.getNamedItem("shell").getNodeValue();
            EnumShell shell = EnumShell.valueOf(stringAux.toUpperCase());

            secondaryGroups = new ArrayList<>();
            nodeAux = attrMap.getNamedItem("secondary_groups");
            if (nodeAux != null){
                stringAux = nodeAux.getNodeValue();
                if (stringAux != null) {
                    stringAux2 = stringAux.split(" ");
                    for (String stringGroup : stringAux2) {
                        stringAux = stringGroup.split("G")[1];
                        intAux = Integer.parseInt(stringAux);
                        secondaryGroups.add(getGroupById(intAux));
                    }
                }
            }

            String passwd = "";
            String userName = "";
            String homePath = "";

            String completeName = null;

            for (int j = 0; j < elements.getLength(); j++) {
                Node node = elements.item(j);
                switch(node.getNodeName()) {
                    case "user_name":
                        userName = node.getTextContent();
                        break;
                    case "password":
                        passwd = node.getTextContent();
                        break;
                    case "home_path":
                        homePath = node.getTextContent();
                        break;
                    case "complete_name":
                        completeName = node.getTextContent();
                        break;
                    default:
                        break;

                }
            }

            createNewUser(userName, userId, passwd, Paths.get(homePath),
                    completeName, shell, mainGroup, 
                    secondaryGroups.stream().toArray(Group [] :: new));

        }		
    }
    
    private void resetSystemData () {
        User.resetSystemUsers();
        Group.resetSystemGroups();
    }
    
    /**
     * Carga un sistema con el contenido del fichero xml contenido en el path.
     * @param pathToXML ruta del fichero xml con el contenido del sistema.
     */
    @Override
    public void loadFrom(Path pathToXML) {
        resetSystemData();
        
        InputSource source;
        DocumentBuilderFactory parserFactory = DocumentBuilderFactory.newInstance();
        parserFactory.setValidating(true);

        try{
            DocumentBuilder parserBuilder = parserFactory.newDocumentBuilder();
            
            parserBuilder.setErrorHandler(new ErrorHandler() {
                @Override
                public void warning(SAXParseException exception) throws SAXException {
                    throw new SAXException("parse warning, the xml file wouldn't be loaded");
                }

                @Override
                public void error(SAXParseException exception) throws SAXException {
                    throw new SAXException("parse error, the xml file wouldn't be loaded");
                }

                @Override
                public void fatalError(SAXParseException exception) throws SAXException {
                    throw new SAXException("parse fatal error, can't load the xml file");
                }
            });
            
            source  =  new InputSource(new FileReader(pathToXML.toString()));
            Element rootElement = parserBuilder.parse(source).getDocumentElement();
            
            NodeList unparsedGroups = rootElement.getElementsByTagName(GROUP_TAG);
            NodeList unparsedUsers = rootElement.getElementsByTagName(USER_TAG);

            parseGroups(unparsedGroups);
            parseUsers(unparsedUsers);

            this.xmlLoaded = true;
            this.modifiedAfterLoaded = false;
            
            if (this.errorOnLoad) {
                resetSystemData();
                this.xmlLoaded = false;
            }
        }catch(ParserConfigurationException | SAXException | IOException e){
            System.err.println(e);
            resetSystemData();
        }
    }

    /**
     * Guarda un sistema cargado en un fichero xml en el path de destino.
     * @param pathToXML ruta del fichero xml donde guardar el contenido del sistema.
     */
    @Override
    public void updateTo(Path pathToXML) {
        FileWriter fw;
        PrintWriter pw;

        try{
            fw = new FileWriter(pathToXML.toString());
            pw = new PrintWriter(fw);
            pw.println(XML_HEADER);
            pw.println(XML_DTD);
            pw.println("<" + ROOT_TAG + ">");

            pw.print(Group.printSystemGroups());
            pw.print(User.printSystemUsers());

            pw.println("</"+ ROOT_TAG + ">");

            pw.close();
            fw.close();
        }catch(IOException io) {
            System.err.println(io);
        }
    }

    /**
     * Devuelve el estado (cargado o no) del sistema.
     * @return true si el sistema ha sido cargado y false en caso contrario.
     */
    @Override
    public boolean isXMLLoaded() {
        return xmlLoaded;
    }

    /**
     * Devuelve el estado (modificado o no) del sistema.
     * @return true si el sistema ha sido modificado despues de la carga y false en caso contrario.
     */
    @Override
    public boolean isModifiedAfterLoaded() {
        return modifiedAfterLoaded;
    }

    /**
     * Devuelve el usuario con el id proporcionado del sistema.
     * @param uId userId del usuario.
     * @return el usuario con el id proporcionado del sistema.
     * o null si el usuario no se encuentra en el sistema.
     */
    @Override
    public User getUserById(int uId) {
        ArrayList<User> users = User.getSystemUsers();
        for(User user : users){
            if(user.getId() == uId){
                return user;
            }
        }
        return null;
    }

    /**
     * Devuelve el usuario con el nombre proporcionado del sistema.
     * @param name nombre del usuario.
     * @return el usuario con el nombre proporcionado del sistema,
     * o null si el usuario no se encuentra en el sistema.
     */
    @Override
    public User getUserByName(String name) {
        ArrayList<User> users = User.getSystemUsers();
        for(User u : users){
            if(u.getName().equals(name)){
                return u;
            }
        }
        return null;
    }

    /**
     * Devuelve el grupo con el id proporcionado del sistema.
     * @param gId id del grupo.
     * @return el grupo con el id proporcionado del sistema.
     * o null si el grupo no se encuentra en el sistema.
     */
    @Override
    public Group getGroupById(int gId) {
        ArrayList<Group> groups = Group.getSystemGroups();
        for( Group g: groups) {
            if(g.getId() == (gId)){
                return g;
            }
        }
        return null;
    }

    /**
     * Devuelve el grupo con el nombre proporcionado del sistema.
     * @param name nombre del grupo.
     * @return el grupo con el nombre proporcionado del sistema,
     * o null si el grupo no se encuentra en el sistema.
     */
    @Override
    public Group getGroupByName(String name) {
        ArrayList<Group> groups = Group.getSystemGroups();
        for(Group g : groups){
            if(g.getName().equals(name)){
                return g;
            }
        }
        return null;
    }

    /**
     * Crea un nuevo usuario en el sistema con los datos proporcionados.
     * @param name nombre del usuario.
     * @param uId userId del usuario.
     * @param password contraseña del usuario.
     * @param pathToHome ruta al home del usuario.
     * @param fullName nombre completo del usuario.
     * @param shell consola del usuario.
     * @param mainGroup grupo principal del usuario.
     * @param secundaryGroups grupos secundarios del usuario.
     */
    @Override
    public void createNewUser(String name, int uId, String password, 
            Path pathToHome, String fullName, EnumShell shell, 
            Group mainGroup, Group[] secundaryGroups) {
        
        try {
            User newUser = new User(name, uId, password, pathToHome, fullName, 
                    shell, mainGroup, secundaryGroups);
            mainGroup.addUser(newUser);

            for (Group group: secundaryGroups) {
                  group.addUser(newUser);
            }

            User.getSystemUsers().add(newUser);
            modifiedAfterLoaded = true;
        } catch (IllegalArgumentException e) {
            System.err.println(e);
            if (!isXMLLoaded()) {
                this.errorOnLoad = true;
            }
        }
    }
    
    /**
     * Crea un nuevo grupo en el sistema con los datos proporcionados.
     * @param name nombre del nuevo grupo.
     * @param gId id del nuevo grupo.
     */
    @Override
    public void createNewGroup(String name, int gId) {
        try {
            Group newGroup = new Group(name, gId);
            Group.getSystemGroups().add(newGroup);
            modifiedAfterLoaded = true;
        } catch (IllegalArgumentException e) {
            System.err.println(e);
            if (!isXMLLoaded()) {
                this.errorOnLoad = true;
            }
        }
    }
    
    /**
     * Añade un usuario del sistema a un grupo del sistema.
     * El usuario no debe pertenecer a ese grupo.
     * @param user usuario a añadir.
     * @param group grupo al que añadir el usuario.
     */
    @Override
    public void addUserToGroup(User user, Group group) {
        try {
            group.addUser(user);
            user.addSecondaryGroup(group);
            modifiedAfterLoaded = true;
        } catch (IllegalArgumentException e) {
            System.err.println(e);
            if (!isXMLLoaded()) {
                this.errorOnLoad = true;
            }
        }
    }
        
    /**
     * Elimina un usuario del sistema de un grupo del sistema.
     * El usuario debe pertenecer al grupo.
     * El grupo no debe ser el grupo principal del usuario.
     * @param user usuario a eliminar.
     * @param group grupo del que eliminar al usuario.
     */
    @Override
    public void removeUserFromGroup(User user, Group group) {
        try{
            group.removeUser(user);
            user.removeSecondaryGroup(group);
            modifiedAfterLoaded = true;
        } catch (IllegalArgumentException e) {
            System.err.println(e);
        }
    }
    
    /**
     * Elimina un usuario del sistema.
     * @param user usuario a eliminar.
     */
    @Override
    public void removeUserFromSystem(User user) {
        Group dummyGroup = new Group("dummy", -1);
        Group aux = user.getMainGroup();
        
        user.setMainGroup(dummyGroup);
        aux.removeUser(user);
        for (Group group: user.getSecondaryGroups()) {
            group.removeUser(user);
        }
        User.getSystemUsers().remove(user);
        
        modifiedAfterLoaded = true;
    }
    
    /**
     * Elimina un grupo del sistema.
     * El grupo a eliminar no puede ser el grupo principal de ningun usuario del sistema.
     * @param group grupo a eliminar del sistema
     */
    @Override
    public void removeGroupFromSystem(Group group) {
        try {
            prepareGroupToRemoveFromSystem(group);
            for (User user: group.getMembers()) {
                user.removeSecondaryGroup(group);
            }
            Group.getSystemGroups().remove(group);
            modifiedAfterLoaded = true;
        } catch (IllegalArgumentException e) {
            System.err.println(e);
        }
    }
    
    /**
     * Devuelve si es posible o no eliminar un grupo del sistema.
     * @param group {@link Group} a eliminar.
     * @return si es posible o no eliminar el grupo del sistema.
     */
    public boolean canRemoveGroupFromSystem(Group group) {
        for (User user: group.getMembers()) {
            if (user.getMainGroup().getId() == group.getId()){
                return false;
            }
        }
        
        return true;
    }
}
