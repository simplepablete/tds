package uva.tds.pr1.equipo03.system;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Pablo Carrascal Muñoz
 * @author Victor Lara Mongil
 * 
 * Clase que representa un usuario en el sistema.
 */
public class User implements Comparable<User>{
    private String name;
    private String password;
    private String homePath;
    private String completeName;

    private EnumShell shell;
    private int id;
    private Group mainGroup;
    private final ArrayList<Group> secondaryGroups;

    private static ArrayList<User> systemUsers = new ArrayList<>();
    
    /**
     * Metodo protegido para reiniciar el sistema, eliminando todos los 
     * usuarios que contenga.
     */
    protected static void resetSystemUsers () {
        systemUsers = new ArrayList<> ();
    }
    
    /**
     * Devuelve un {@link ArrayList} con todos los usuarios en el sistema.
     * @return {@link ArrayList} con todos los usuarios en el sistema.
     */
    public static ArrayList<User> getSystemUsers(){
        return systemUsers;
    }
    
    /**
     * Devuelve la informacion de los usuarios en el sistema en formato xml.
     * @return {@link String} con la informacion de los usuarios en el sistema en formato xml.
     */
    public static String printSystemUsers () {
        String users = "";

        for (User user: User.getSystemUsers()){
            users += "    <user user_ID=\"U" + user.getId() + "\" ";
            users += "shell=\"" + user.getShell().getValue() + "\" ";
            users += "main_group=\"G" + user.getMainGroup().getId() + "\" ";

            if (!user.getSecondaryGroups().isEmpty()) {
                users += "secondary_groups=\"";
                for (Group group: user.getSecondaryGroups()){
                    users += "G" + group.getId() + " ";
                }
                users += "\" ";
            }

            users += ">\n";
            users += "        <user_name>" + user.getName() + "</user_name>\n";
            users += "        <password>" + user.getPassword() + "</password>\n";
            users += "        <home_path>" + user.getHomePath() + "</home_path>\n";
        
            if (user.getCompleteName() != null) {
                users += "        <complete_name>" + user.getCompleteName() +"</complete_name>\n";
            }
            
            users += "    </user>\n";
        }
        return users;
    }
    
    private void validateData(String name, int id) throws IllegalArgumentException {
        if (id < 1) {
            System.out.println("user" + id);
            throw new IllegalArgumentException("Id must be positive. The operation failed.");
        }
        
        for (User user: User.getSystemUsers()) {
            if (user.getId() == id || user.getName().equals(name)) {
                throw new IllegalArgumentException("There is a user with the same id or name. The operation failed.");
            }
        }
    }
    
    private void validateNewName(String name) throws IllegalArgumentException {
        for (User user: User.getSystemUsers()) {
            if (user.getName().equals(name)) {
                throw new IllegalArgumentException("There is a user with the same name. The operation failed.");
            }
        }
    }
    
    
    
    /**
     * Crea un nuevo usuario en el sistema con todos sus datos asociados.
     * El nombre y el userId deben ser unicos. El id debe ser positivo.
     * @param name nombre del usuario.
     * @param uId userId del usuario.
     * @param password contraseña del usuario.
     * @param homePath home path del usuario.
     * @param fullName nombre completo del usuario.
     * @param shell consola del usuario.
     * @param mainGroup grupo principal al que pertenece el usuario.
     * @param secundaryGroups otros grupos a los que pertenece el usuario.
     * @throws IllegalArgumentException si el nombre o el userId no cumplen con los requisitos.
     */
    public User(String name, int uId, String password, Path homePath, String fullName, 
            EnumShell shell,Group mainGroup, Group[] secundaryGroups) throws IllegalArgumentException {
        validateData(name, uId);
        this.name = name;
        this.id = uId;
        this.password = password;
        this.homePath = homePath.toString();
        this.completeName = fullName;
        this.mainGroup = mainGroup;
        this.secondaryGroups = new ArrayList<>(Arrays.asList(secundaryGroups));
        
        if(shell == null){
            this.shell = EnumShell.BASH;
        }
        else {
            this.shell = shell;
        }
    }
    
    /**
     * Devuelve el nombre del usuario.
     * @return el nombre del usuario.
     */
    public String getName() {
        return name;
    }

    /**
     * Fija un nuevo nombre para el usuario en el sistema. 
     * El nuevo nombre debe ser unico en el sistema.
     * @param name nuevo nombre del usuario.
     */
    public void setName(String name) {
        try {
            validateNewName(name);
            this.name = name;
        } catch (IllegalArgumentException e) {
            System.err.println(e);
        }
    }

    /**
     * Devuelve la contraseña del usuario.
     * @return contraseña del usuario
     */
    public String getPassword() {
        return password;
    }

    /**
     * Fija una nueva contraseña para el usuario.
     * @param password nueva contraseña para el usuario.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Devuelve el home path del usuario.
     * @return home path del usuario.
     */
    public String getHomePath() {
        return homePath;
    }
    
    /**
     * Fija un nuevo home path para el usuario.
     * @param homePath nuevo home path para el usuario.
     */
    public void setHomePath(String homePath) {
        this.homePath = homePath;
    }

    /**
     * Devuelve el nombre completo del usuario.
     * @return nombre completo del usuario.
     */
    public String getCompleteName() {
        return completeName;
    }

    /**
     * Fija un nuevo nombre completo para el usuario.
     * @param completeName nuevo nombre completo para el usuario.
     */
    public void setCompleteName(String completeName) {
        this.completeName = completeName;
    }
    
    /**
     * Devuelve la consola del usuario.
     * @return conolsa del usuario.
     */
    public EnumShell getShell() {
        return shell;
    }

    /**
     * Fija una nueva consola para el usuario.
     * @param shell nueva consola para el usuario.
     */
    public void setShell(EnumShell shell) {
        this.shell = shell;
    }

    /**
     * Devuelve el id del usuario.
     * @return id del usuario.
     */
    public int getId() {
        return id;
    }

    /**
     * Devuelve el grupo principal del usuario.
     * @return {@link Group} principal del usuario.
     */
    public Group getMainGroup() {
        return mainGroup;
    }

    /**
     * Fija un nuevo grupo principal para el usuario.
     * @param mainGroup nuevo {@link Group} principal para el usuario.
     */
    public void setMainGroup(Group mainGroup) {
        this.mainGroup = mainGroup;
    }

    /**
     * Devuelve la lista de grupos secundarios del usuario.
     * @return {@link ArrayList} con los {@link Group} secundarios del usuario.
     */
    public ArrayList<Group> getSecondaryGroups() {
        return secondaryGroups;
    }
    
    /**
     * Añade un nuevo grupo a la lista de grupos secundarios del usuario.
     * El grupo no debe estar en la lista de grupos secundarios del usuario, 
     * ni ser su grupo principal.
     * @param group {@link Group} a añadir a los grupos secundarios del usuario.
     * @throws IllegalArgumentException si el grupo es uno de los grupos 
     * secundarios del usuario, o si es el grupo principal del usuario.
     */
    public void addSecondaryGroup(Group group) throws IllegalArgumentException {
        if (this.mainGroup.getId() == group.getId()) {
            throw new IllegalArgumentException("this user has already this group "
                    + "as a main group. The operation failed");
        }
        for (Group secondaryGroup: this.secondaryGroups) {
            if (secondaryGroup.getId() == group.getId()) {
                throw new IllegalArgumentException("this user has already this "
                        + "group as a secondary group. The operation failed");
            }
        }
        this.secondaryGroups.add(group);
    }
    
    /**
     * Elimina un grupo de la lista de grupos secundarios del usuario.
     * El grupo debe estar en la lista de grupos secundarios del usuario.
     * @param group {@link Group} a eliminar de los grupos secundarios del usuario.
     * @throws IllegalArgumentException  si el grupo no es un grupo secundario del usuario.
     */
    public void removeSecondaryGroup(Group group) throws IllegalArgumentException {
        boolean isUserInGroup = false;
        
        for (Group secondaryGroup: this.getSecondaryGroups()) {
            if (secondaryGroup.getId() == group.getId()) {
                isUserInGroup = true;
            }
        }
        
        if (!isUserInGroup) {
            throw new IllegalArgumentException("this user hasn't this group as a secondary group. The operation failed");
        }
        
        this.secondaryGroups.remove(group);
    }
    
    /**
     * Implementacion de la interfaz Comparable\<User\> que permite comparar
     * usuarios respecto de su id.
     * @param user {@link User} a comparar.
     * @return el resultado de restar los ids de los usuarios. 
     */
    @Override
    public int compareTo(User user) {
        return (this.id - user.getId());
    }
}
