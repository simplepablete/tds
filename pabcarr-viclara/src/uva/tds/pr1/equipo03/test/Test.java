package uva.tds.pr1.equipo03.test;

import java.nio.file.Path;
import java.nio.file.Paths;
import uva.tds.pr1.equipo03.system.EnumShell;
import uva.tds.pr1.equipo03.system.Group;
import uva.tds.pr1.equipo03.system.User;

import uva.tds.pr1.equipo03.system.UserSystemImpl;

public class Test {
	
    private UserSystemImpl implementation;
    private Path loadPath;
    private Path savePath;
    
    public Test (String loadPath) {
        System.out.println(loadPath);
        this.loadPath = Paths.get(loadPath);
        savePath = Paths.get(loadPath + ".res");
        this.implementation = new UserSystemImpl();
    }
    
    /**
     * Need Test3.xml file
     */
    public void modifiedSysemTest() {
        setUp();
        createNewGroup();
        modifyGroup();
        createNewUser();
        modifyUser();
        tearDown();
        System.out.println("modify test acabados");
    }
    
    public void loadAndReloadTest() {
        setUp();
        this.implementation.updateTo(this.savePath);
        this.implementation.loadFrom(this.savePath);
        System.out.println("load and reload test acabados");
        tearDown();
    }
    
    private void createNewGroup() {
        this.implementation.createNewGroup("test", 6);
        if(!(this.implementation.getGroupById(6) != null && 
                this.implementation.getGroupById(6).getId() == 6 &&
                this.implementation.getGroupById(6).getName().equals("test"))) {
            System.out.println("createNewGroup failed");
        }
    }
    
    private void createNewUser() {
        this.implementation.createNewUser("test", 9, "passwd", Paths.get("/home/test"), "testfullname", 
                EnumShell.ASH, this.implementation.getGroupById(6), new Group [0]);
        if(!(this.implementation.getUserById(9) != null && 
                this.implementation.getUserById(9).getId() == 9 &&
                this.implementation.getUserById(9).getName().equals("test"))) {
            System.out.println("createNewUser failed");
        }
    }
    
    private void modifyGroup () {
        Group group = this.implementation.getGroupById(6);
        if (group != null) {
            group.setName("newTestName");
            this.implementation.updateTo(this.savePath);
            this.implementation.loadFrom(this.savePath);
            if (!(this.implementation.getGroupById(6) != null &&
                    this.implementation.getGroupById(6).getName().equals("newTestName"))) {
                System.out.println("modifyGroup test failed");
            }
        }
    }
    
    private void modifyUser () {
        User user = this.implementation.getUserById(9);
        
        if (user != null) {
            user.setName("newTestName");
            this.implementation.updateTo(this.savePath);
            this.implementation.loadFrom(this.savePath);
            
            if (!(this.implementation.getUserById(9) != null &&
                    this.implementation.getUserById(9).getName().equals("newTestName"))) {
                System.out.println("modifyUser test failed");
            }
        }
    }
    /**
     * Need the Test3.xml file
     */
    public void gettersTest() {
        testGetUserById();
        testGetUserByIdnull();
        testGetUserByName();
        testGetUserByNameNull();
        testGetGroupByID();
        testGetGroupByIDNull();
        testGetGroupByName();
        testGetGroupByNameNull();
        System.out.println("getters test acabados");
    }
	
    private void testGetGroupByIDNull(){
        setUp();
        if(this.implementation.getGroupById(6) != null) {
            System.out.println("testGetGrupoByIDNull Falla");
        }
        tearDown();
    }	
	
    private void testGetGroupByID(){
        setUp();
        if(!(this.implementation.getGroupById(4) != null && 
                this.implementation.getGroupById(4).getId() == 4)) {
            System.out.println("testGetGrupoByID Falla");
        }
        tearDown();
    }
	
    private void testGetGroupByName(){
        setUp();
        if(!(this.implementation.getGroupByName("group_name3") != null &&
                this.implementation.getGroupByName("group_name3").getId() == 3)) {
            System.out.println("testGetGrupoByName Falla");
        }
        tearDown();
    }
	
    private void testGetGroupByNameNull(){
        setUp();
        if(!(this.implementation.getGroupByName("grup_name7") == null)) {
            System.out.println("testGetGrupoByNameNull Falla");
        }
        tearDown();
    }
	
    private void testGetUserByNameNull(){
        setUp();
        if(!(this.implementation.getUserByName("user_name17") == null)) {
            System.out.println("testGetUserByNameNull Falla");
        }
        tearDown();
    }
	
    private void testGetUserByName(){
        setUp();
        if(!(this.implementation.getUserByName("user_name3") != null &&
                this.implementation.getUserByName("user_name3").getName().equals("user_name3"))) {
            System.out.println("testGetUserByName Falla");
        }
        tearDown();
    }

    private void testGetUserByIdnull(){
        setUp();
        if(!(this.implementation.getUserById(5) == null)) {
            System.out.println("testGetUserByIdNull Falla");
        }
        tearDown();
    }
	
    private void testGetUserById(){
        setUp();
        if(!(this.implementation.getUserById(3) != null &&
                this.implementation.getUserById(3).getName().equals("user_name3"))) {
            System.out.println("testGetUserById Falla");
        }
        tearDown();
    }
	
    private void setUp() {
        this.implementation.loadFrom(this.loadPath);
    }

    private void tearDown() {
        if (this.implementation.isXMLLoaded()) {
            this.implementation.updateTo(this.savePath);
        }
    }	
}
