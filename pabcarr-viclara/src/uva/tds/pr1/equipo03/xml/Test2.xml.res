<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE groups_users_unix SYSTEM "src/uva/tds/pr1/equipo03/xml/groups_users_unix.dtd">
<groups_users_unix>
    <group group_ID="G1" members="U1 U3 U4 " >
        <group_name>group_name</group_name>
    </group>
    <group group_ID="G2" members="U1 U2 " >
        <group_name>group_name1</group_name>
    </group>
    <group group_ID="G3" members="U2 U3 " >
        <group_name>group_name2</group_name>
    </group>
    <group group_ID="G4" members="U1 U2 U3 " >
        <group_name>group_name3</group_name>
    </group>
    <user user_ID="U1" shell="Zsh" main_group="G1" secondary_groups="G4 G2 " >
        <user_name>user_name</user_name>
        <password>password</password>
        <home_path>home_path</home_path>
        <complete_name>complete_name</complete_name>
    </user>
    <user user_ID="U2" shell="bash" main_group="G2" secondary_groups="G3 G4 " >
        <user_name>user_name2</user_name>
        <password>password</password>
        <home_path>home_path</home_path>
        <complete_name>complete_name</complete_name>
    </user>
    <user user_ID="U3" shell="ksh" main_group="G3" secondary_groups="G4 G1 " >
        <user_name>user_name3</user_name>
        <password>password</password>
        <home_path>home_path</home_path>
        <complete_name>complete_name</complete_name>
    </user>
    <user user_ID="U4" shell="csh" main_group="G1" >
        <user_name>user_name4</user_name>
        <password>password</password>
        <home_path>home_path</home_path>
    </user>
</groups_users_unix>
